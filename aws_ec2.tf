# Create a VPC

resource "aws_vpc" "demo_vpc"{
  cidr_block = "192.168.0.0/16"
  tags = {
    "Name" = "new_vpc"
  }
}

resource "aws_subnet" "demosub" {
  vpc_id     = aws_vpc.new_vpc.id
  cidr_block = "192.168.0.0/24"

    tags = {
     Name = "mysub"
  }
}


# Create Internet Gateway

resource "aws_internet_gateway" "IGW" {
  vpc_id = aws_vpc.new_vpc.id

tags = {
    Name = "IGW"
  }
}

# Create a Security Group

resource "aws_security_group" "SG_New" {
  name        = "allow_traffic"
  description = "Allow inbound and outbound traffic"
  vpc_id      = aws_vpc.new_vpc.id

ingress {
    description = "TLS from VPC"
    from_port   = 0
    to_port     = 10000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

tags = {
    Name = "allow_traffic"
  }
}

# Create route table and association

resource "aws_route_table" "RT_New" {
    vpc_id = aws_vpc.new_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.IGW.id
    }

    tags = {
        Name = "RT_New"
    }
}

# Create route table association
resource "aws_route_table_association" "RT-Association" {
    subnet_id = aws_subnet.mysub.id
    route_table_id = aws_route_table.RT_New.id
}

# Create an AWS EC2 Instance

resource "aws_instance" "new_ec2" { 
  ami           = "ami-089539692cca55c6c" 
  instance_type = "t2.micro"
  key_name = "london123"
  vpc_security_group_ids = [aws_security_group.SG_New.id]
  subnet_id = aws_subnet.mysub.id
  associate_public_ip_address = true
  user_data = file ("./Install_Jenkins.sh")
 
  
  tags = {
    Name = "terraform_ec2"
  }

}
