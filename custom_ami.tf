# The "AMI from instance" resource allows the creation of an Amazon Machine Image (AMI) modelled after an existing EBS-backed EC2 instance.

# The created AMI will refer to implicitly-created snapshots of the instance's EBS volumes and mimick its assigned block device configuration at the time the resource is created.

resource "aws_ami_from_instance" "custom_ami"{
  name               = "custom_ec2_ami"
  source_instance_id = "i-0e29d5fcfb39c26c4"
}
